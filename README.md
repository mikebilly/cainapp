# CAINAPP 2 BETA🚧

Welcome to CAINAPP 2.0!

### What is CAIN?

##### CAIN - Channel Attention Is All You Need for Video Frame Interpolation

CAIN is a very fast interpolation network that doesn't use much vram (Ex: 5GB with fp16 for 8k)! 



### What is cainapp?

Cainapp is a gui for cain with some new things like fp16, saving frames on other threads and more!



#### any questions or bugs?

Write to me on discord hubert#6969 or ask on cainapp discord https://discord.gg/HVMBnWjmxP



## What are the requirements?

Unlike previously, Cainapp now runs on CUDA (experimental - directml). 

However, we cannot guarantee that you will have the best experience with lower specced hardware, so we at least suggest getting a GPU with over 2GB of VRAM for at least a decent experience.\



## How to install on Windows


1. Download Python 3.9 (or 3.8 on windows 7) (https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe)

2. Install Python 3.9, and remember to select "Add to PATH"

3. In cmd run 

   `pip3 install pyqt5`

   `pip3 install qdarkstyle`

   `pip install pyqt5-tools`

   `pip3 install --pre torch torchvision torchaudio -f https://download.pytorch.org/whl/nightly/cu111/torch_nightly.html` this can need much time 

   `pip3 install tqdm`

   `pip3 install opencv-python`

4. download cainapp https://gitlab.com/hubert.sontowski2007/cainapp/-/archive/main/cainapp-main.zip and unzip it

5. In cmd, run ` cd path/to/cainapp/ ` then ```python main.py```

   

## How to install on Debian/Ubuntu-based distributions


1. Install git ```sudo apt install git```

2. Clone cainapp ```git clone https://gitlab.com/hubert.sontowski2007/cainapp```

3. Install python 3 pip ```sudo apt install python-pip```

4. In terminal run 

   `pip3 install pyqt6`

   `pip3 install qdarkstyle`

   `pip3 install torch==1.8.1+cu111 torchvision==0.9.1+cu111 torchaudio===0.8.1 -f https://download.pytorch.org/whl/torch_stable.html` this can need much time ⌚

   `pip3 install tqdm`

   `pip3 install opencv-python-headless`

   `sudo apt install ffmpeg`

5. In terminal, run ` cd cainapp/ ` then ```python3 main.py```


Just as a precaution, this code is completely untested on Debian. If you have any feedback, let us know!

## How to run the Discord bot🤖

Basically the same as in "how to install" but you need to install 2 more things and do some arranging

`pip3 install discord.py` 

`pip3 install requests`


Take your model file from before, copy it to a directory named `models`

And run ```python3 bot.py --token (here token)```



## Donate💰

If you wish to support the project, you can donate to Hubert's patreon [here](https://www.patreon.com/hubert_)!


## CAIN Citation

```
@inproceedings{choi2020cain,
    author = {Choi, Myungsub and Kim, Heewon and Han, Bohyung and Xu, Ning and Lee, Kyoung Mu},
    title = {Channel Attention Is All You Need for Video Frame Interpolation},
    booktitle = {AAAI},
    year = {2020}
}
```

