import modules.generate as generate
import os
import glob
import time
def rename(dir_path,filetype="png"):
    var1=sorted(os.listdir(dir_path))
    startnum=0
    for file in var1:
        os.rename(file,dir_path +"/"+ str(startnum).zfill(6)+filetype)
        startnum+=1

for filename in os.listdir("input"):

    try:
        files = glob.glob('temp/*')
        for f in files:
            os.remove(f)
    except:
        print("temp don't exist")
        os.mkdir("temp")

    
    try:
        os.mkdir("output")
    except:
        print("output")
    os.system(f"ffmpeg -i 'input/{filename}' -vf scale=848:480 -q 0 temp/%6d.jpg")
    try:
        os.remove("1.wav")
    except:
        print("no 1.wav")
    os.system(f"ffmpeg -i 'input/{filename}' 1.wav")

    generate.interpolation(batch_size=4, temp_img = "temp", fp16=True, modelp="1.pth",img_fmt="jpg",cli=True)
    rename("temp",filetype=".jpg")
    time.sleep(4)
    os.system(f"ffmpeg -i temp/%6d.jpg -i 1.wav -c:v libx265 -preset ultrafast -crf 17 'output/{filename}.mkv'")
