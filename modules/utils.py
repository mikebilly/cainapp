import os
import sys
import numpy as np
import torch
import cv2
from torchvision import transforms
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from PIL import Image, ImageFont, ImageDraw
from io import BytesIO 
import kornia
def load_dataset(data_root, batch_size, test_batch_size, num_workers, test_mode='medium', img_fmt='png',dataloader="new"):
    if dataloader=="new":
        from modules.newloader import get_loader
    if dataloader=="old":
        from modules.oldloader import get_loader

    test_loader = get_loader('test', data_root, test_batch_size, img_fmt=img_fmt, shuffle=False, num_workers=num_workers, n_frames=1)
    #train_loader = get_loader('train', data_root, batch_size, shuffle=True, num_workers=num_workers)
    return  test_loader

def build_input(images, imgpaths, is_training=True, include_edge=False, device=torch.device('cuda')):
    if isinstance(images[0], list):
        images_gathered = [None, None, None]
        for j in range(len(images[0])):  # 3
            _images = [images[k][j] for k in range(len(images))]
            images_gathered[j] = torch.cat(_images, 0)
        imgpaths = [p for _ in images for p in imgpaths]
        images = images_gathered

    im1, im2 = images[0].to(device), images[2].to(device)
    gt = images[1].to(device)

    return im1, im2, gt




def quantize(img, rgb_range=255):
    return img.mul(255 / rgb_range).clamp(0, 255).round()


def save_image(img, path):
    # img : torch Tensor of size (C, H, W)
    img=kornia.color.yuv.yuv_to_rgb(img)
    q_im = quantize(img.data.mul(255))
    
    if len(img.size()) == 2:    # grayscale image
        im = Image.fromarray(q_im.cpu().numpy().astype(np.uint8), 'L')
    elif len(img.size()) == 3:
        im = Image.fromarray(q_im.permute(1, 2, 0).cpu().numpy().astype(np.uint8), 'RGB')
    else:
        pass
    
    im = np.array(im)
    cv2.imwrite(path,im)



