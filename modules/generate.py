import os
import sys
import time
import copy
import threading
import torch
import platform
import os
import numpy as np
from tqdm import tqdm
import modules.utils as utils
import cv2
import modules.newloader as newloader

from modules.utils import quantize
from PIL import Image
@torch.inference_mode()
def interpolation(batch_size=1, img_fmt="png", torch_device="cuda", temp_img = "frameseq/", GPUid=0, GPUid2=2, fp32=True, modelp="1.pth", TensorRT=False,  appupdate=False, app="hmm", dataloader="new",partialconv2d=False, funnynumber=1,cli=False,onnx=False,width=1920,height=1080,newmodel=True):    #torch.cuda.set_device(GPUid)
    ossystem=platform.system()
    print(ossystem)
    torch.cuda.device(GPUid)
    torch.backends.cudnn.benchmark = True
    if onnx:
        from modules.cain import CAIN
        import onnxruntime as rt
        try:
            os.remove("temp.onnx")
        except:
            pass
        size = (batch_size,3,height,width)
        dummy_input1 = torch.randn(size)
        dummy_input2 = torch.randn(size)
        input_names = ["input_1", "input_2"]
        output_names = ["output_frame"]
        model = CAIN(3)
        model.load_state_dict(torch.load(modelp),strict=False)
        print("converting model...")
        torch.onnx.export(model, (dummy_input1,dummy_input2), f"temp.onnx", verbose=False, input_names=input_names, output_names=output_names, dynamic_axes={'input' : {0 : 'batch_size'}, 'input' : {2 : 'height'},'input' : {3 : 'width'},'output' : {0 : 'batch_size'},'output' : {2 : 'height'},'output' : {3 : 'width'}})
        sess = rt.InferenceSession("temp.onnx")
        input_name = sess.get_inputs()[0].name
        input_name1 = sess.get_inputs()[1].name
        print("input name", input_name)
        input_shape = sess.get_inputs()[0].shape
        print("input shape", input_shape)
        input_type = sess.get_inputs()[0].type
        print("input type", input_type)
        output_name = sess.get_outputs()[0].name
        print("output name", output_name)
        output_shape = sess.get_outputs()[0].shape
        print("output shape", output_shape)
        output_type = sess.get_outputs()[0].type
        print("output type", output_type)
    else:
        device = torch.device(torch_device)
        with torch.no_grad():
            model = torch.jit.load(modelp)
        for param in model.parameters():
            param.grad = None
        torch.autograd.set_detect_anomaly(False)
        torch.autograd.profiler.profile(False)
        torch.autograd.profiler.emit_nvtx(False)
        torch.backends.cuda.matmul.allow_tf32 = True
        torch.backends.cudnn.allow_tf32 = True
        if fp32==True:
            model.float()
        else:
            model.half()
    if ossystem=='Linux':
        def save():
            utils.save_image(out[b], temp_img+"/"+savepath)
    else:
        def save():
            if cli==True:
                utils.save_image(out[b], savepath)
            else:
                utils.save_image(out[b], temp_img[:-6]+savepath)
    @torch.inference_mode()
    def test():
        global savepath
        global images
        global fInd

        global fpos
        global meta
        global out
        global b
        global tsave
        ##### Load Dataset #####
        if ossystem=='Linux':
            test_loader = utils.load_dataset(
                temp_img, batch_size, batch_size, 0, img_fmt=img_fmt, dataloader=dataloader)
        else:
            test_loader = utils.load_dataset(
                temp_img, batch_size, batch_size, 0, img_fmt=img_fmt, dataloader=dataloader)
        for i, (images, meta) in enumerate(tqdm(test_loader, ascii=True)):
            # Build input batch
            if onnx:
                im1, im2 = images[0], images[1]
                out=sess.run([output_name], {input_name: np.array(im1), input_name1: np.array(im2)})
                out=torch.tensor(out[0]).float()
            else:

                if fp32:
                    im1, im2 = images[0].to(device), images[1].to(device)
                else:
                    im1, im2 = images[0].to(device, dtype=torch.float16), images[1].to(device, dtype=torch.float16)
                if newmodel:
                    out= model(im1, im2)
                else:
                    out,_= model(im1, im2)
                
            
            for b in range(images[0].size(0)):
                paths = meta['imgpath'][0][b].split('/')
                fp = temp_img
                fp = os.path.join(paths[-1][:-4])   # remove '.png' extension

                # Decide float index
                i1_str = paths[-1][:-4]
                i2_str = meta['imgpath'][1][b].split('/')[-1][:-4]
                try:
                    i1 = float(i1_str.split('_')[-1])
                except ValueError:
                    i1 = 0.0
                try:
                    i2 = float(i2_str.split('_')[-1])
                    if i2 == 0.0:
                        i2 = 1.0
                except ValueError:
                    i2 = 1.0
                fpos = max(0, fp.rfind('_'))
                fInd = (i1 + i2) / 2
                savepath = "%s_%06f.%s" % (fp[:fpos], fInd, img_fmt)
                tsave = threading.Thread(target=save)
                tsave.start()        


         
    
    test()
    if dataloader=="new":
        newloader.clean()
    try:
        del model
    except:
        del model_trt
    torch.cuda.empty_cache() 


#
def main():
    interpolation()
    #test()
    #test()
    #test()

if __name__ == "__main__":
    main()
